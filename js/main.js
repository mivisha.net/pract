var app = new Vue({
    el: "#app",
    data: {
        products: [
            { id: 1, title: "Potato 1", short_text: "Corund Red Potato", image: '1.jpeg', desc: '' },
            { id: 2, title: "Potato 2", short_text: "Standart Red Potato", image: '2.jpeg', desc: '' },
            { id: 3, title: "Potato 3", short_text: "Zlata Yellow Potato", image: '3.jpg', desc: '' },
            { id: 4, title: "Potato 4", short_text: "Daikon White Potato", image: '4.jpg', desc: '' },
            { id: 5, title: "Potato 5", short_text: "White Hailstone Potato", image: '5.jpg', desc: '' },
        ],
        product: [{}],
        cart: [],
        btnVisible: 0,
        atLeastOneInCart: 0,
        contactFields: {},
        formVisible: 1,
    },

    methods: {
        getProduct: function () {
            if (window.location.hash) {
                var id = window.location.hash.replace('#', '');
                if (this.products && this.products.length > 0) {
                    for (i in this.products) {
                        if (this.products[i] && this.products[i].id && id == this.products[i].id)
                            this.product = this.products[i];
                    }
                }
            }
        },

        addToCart: function (id) {
            var cart = [];

            if (window.localStorage.getItem('cart')) {
                cart = window.localStorage.getItem('cart').split(',');
            }

            if (cart.indexOf(String(id)) == -1) {
                cart.push(id);
                window.localStorage.setItem('cart', cart.join());
                this.btnVisible = 1;
            }
        },

        checkInCart: function () {
            if (this.product && this.product.id && window.localStorage.getItem('cart').split(',').indexOf(String(this.product.id)) != -1)
                this.btnVisible = 1;
        },

        getCart: function () {
            var lStorage = [];
            lStorage = localStorage.getItem('cart').split(',')
            for (i in this.products) {
                if (lStorage.indexOf(String(this.products[i].id)) != -1) {
                    this.cart.push(this.products[i])
                }
            }
            this.atLeastOneInCart = this.cart.length
        },

        removeFromCart: function (id) {
            var lStorage = [];
            lStorage = window.localStorage.getItem('cart').split(',')

            lStorage = lStorage.filter(storageId => storageId != id)
            window.localStorage.setItem('cart', lStorage.join())

            this.cart = this.cart.filter(item => item.id != id)

            this.atLeastOneInCart = this.cart.length
        },
        
        makeOrder: function () {
            localStorage.clear();
            this.cart.splice(0, this.cart.length)
            this.formVisible = 0
            this.atLeastOneInCart = this.cart.length
        },

    },
    

    mounted: function () {
        this.getProduct()
        this.checkInCart()
        this.getCart();
    },

});
fetch('https://restcountries.eu/rest/v2/regionalbloc/eu')
.then(res => res.json())
.then(data => {
    const Capitals = document.createElement('ul');
    Capitals.classList.add("category-items");
    data.forEach(function(item){
        const viewItem=document.createElement('li');
        viewItem.appendChild(document.createTextNode(item.capital));
        Capitals.appendChild(viewItem);            
    });
    document.getElementsByClassName("list")[0].getElementsByClassName("capitals")[0].appendChild(Capitals);     
    
});
